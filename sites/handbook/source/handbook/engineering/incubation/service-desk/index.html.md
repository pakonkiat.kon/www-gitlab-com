---
layout: handbook-page-toc
title: Service Desk Single-Engineer Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Service Desk Single-Engineer Group

The Service Desk SEG is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation/).

We have a [Service Desk offering](https://about.gitlab.com/direction/plan/certify/#service-desk) in GitLab that we'd like to make an integral part of the GitLab support workflow.

Our aim is to create a convenient and low cost alternative to third party tools by integrating a seamless experience for our customers to bridge the gap between developers and support staff.

