---
title: "Visual Studio code editor: Eight tips for using GitLab VS Code"
author: Tomas Vik
author_gitlab: viktomas
author_twitter: viktomas_com
categories: engineering
image_title: '/images/blogimages/vscode-extension-development-with-gitlab/cover.jpg'
description: "Learn how to use the Visual Studio code editor more efficiently and meet some of the GitLab contributors that made these new features happen."
tags: integrations, tutorial
ee_cta: false
postType: content marketing
featured: yes
twitter_text: "Tips to increase your @GitLab efficiency when working in @code"
related_posts:
  - "/blog/2018/03/01/gitlab-vscode-extension/"
  - "/blog/2020/07/31/use-gitlab-with-vscode/"
  - "/blog/2020/11/30/vscode-extension-development-with-gitlab/"
  - "/blog/2021/01/25/mr-reviews-with-vs-code"
merch_banner: merch_seven
---

As a software engineer, I spend a significant portion of my day in the Visual Studio code editor. Since I started maintaining the officially supported [GitLab VS Code extension](https://marketplace.visualstudio.com/items?itemName=GitLab.gitlab-workflow), I've developed a few tricks that make me a productive GitLab user. Below, I share eight tips that make my work more efficient and productive, while also introducing you to some of the GitLab contributors who made this tooling happen.

## 1. Clone GitLab project

GitLab contributor [Felix Haase](https://gitlab.com/haasef) recently [implemented a feature that lets you clone any GitLab project where you are a member](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/merge_requests/172). To clone the project, use the official `Git: Clone` command and select your GitLab instance. Use the `Git: Clone` command by selecting the command from the [Command Palette](https://code.visualstudio.com/docs/getstarted/userinterface#_command-palette).

This feature can save you time if you already know the name of the project you want to clone.

![VS Code clone dialogue](/images/blogimages/vscode-workflows-for-working-with-gitlab/clone.png){: .shadow.medium.center}
VS Code lets you filter which project to clone.
{: .note .text-center}

## 2. View MRs and issues

It is easy to look through issues and MRs that you created, are assigned to, or are reviewing using GitLab. The lesser-known feature of the GitLab Workflow extension is [custom queries](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/blob/main/docs/user/custom-queries.md). Custom queries allow you to refine the search expressions for issues and MRs that appear in the VS Code side panel. You can apply all criteria you are used to from the GitLab web search: Labels, full-text search expression, milestones, authors, assignees, and more.

![GitLab extension sidebar](/images/blogimages/vscode-workflows-for-working-with-gitlab/issues-and-mrs.png){: .shadow.medium.center}
See your issues and MRs in the VS Code sidebar.
{: .note .text-center}

Another option is [reviewing the MRs in VS Code](/blog/2021/01/25/mr-reviews-with-vs-code/). The final functionality that is missing in MR review is [creating new comments on the MR diff](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues/342), which we plan to ship by July 2021.

## 3. Create an MR with two clicks

If you use the `git` command in a terminal, you might have noticed that pushing your branch to GitLab produces the following output:

```txt
remote: To create a merge request for my-new-branch, visit:
remote:   https://gitlab-instance.com/my-group/my-project/merge_requests/new?merge_request%5Bsource_branch%5D=my-new-branch
```

After clicking the link, the terminal will open your browser on a new MR page where you can create an MR from the branch you just pushed.

When I started pushing my branches through VS Code, I missed this feature. To the point that I searched through the VS Code Git Extension logs to find the create MR link (command `Git: Show Git Output`).

Luckily, GitLab contributor [Jonas Tobias Hopusch](https://gitlab.com/jotoho) implemented a [status bar button](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/merge_requests/155) that lets you create MRs just as easily.

To create an MR from your changes, push them to your remote (the cloud icon next to the branch name) and then click on the `GitLab: Create MR.` button.

![VS Code status bar](/images/blogimages/vscode-workflows-for-working-with-gitlab/status-bar-create-mr.png){: .shadow.medium.center}
VS Code status bar with buttons from GitLab extension.
{: .note .text-center}

## 4. Configure your GitLab CI

The GitLab extension helps you edit your `.gitlab-ci.yml` file in two ways: Autocompleting environment variables and validating the configuration.

Thanks to [Kev's](https://gitlab.com/KevSlashNull) fantastic [contribution](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/merge_requests/140), you can use CI variable autocompletion anywhere in your `.gitlab-ci.yml`. The hints even include variable descriptions and explain supported GitLab versions.

![CI variables autocomlete dialogue](/images/blogimages/vscode-workflows-for-working-with-gitlab/ci-autocomplete.png){: .shadow.medium.center}
CI variables autocomlete dialogue.
{: .note .text-center}

When you finish writing your `.gitlab-ci.yml` CI configuration, you can use the `GitLab: Validate GitLab CI config` command to surface any problems before committing the CI config to your repository.

## 5. Create and paste project snippets

Is there a piece of text that you and your teammates often use? Maybe it is a license header for a file or a test scenario template. You can use GitLab snippets in combination with Visual studio code editor to save you a few keystrokes.

For example, you can create a [test file snippet](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/snippets/2110322) with the `GitLab: Create snippet` command and then paste it into every new test file you create with the `GitLab: Insert snippet` command.

![Paste Snippet dialogue](/images/blogimages/vscode-workflows-for-working-with-gitlab/paste-snippet.png){: .shadow.medium.center}
Paste Snippet dialogue.
{: .note .text-center}

I mostly use snippets when I want to share a big blob of text. I select the text and then create the snippet with the `GitLab: Create snippet` command.

## 6. Copy web URL for a project file

Most of the [communication at GitLab happens asynchronously](/handbook/values/#bias-towards-asynchronous-communication). So instead of being able to show your colleague an open file in your editor, you'll need to be able to create a textual pointer to the file.

A straightforward way to do that is to use the `GitLab: Copy link to active file on GitLab` command, which will copy the web URL of the open file into your clipboard. It even includes the line number or a range of lines based on your cursor or selection in the Visual studio code editor.

You might also consider using the `GitLens: Copy Remote File URL`, which even includes the commit SHA in the URL, making it a permalink. The permalink will always point to the same version of the file regardless of further commits to your branch. We'll look at the GitLens extension in tip number 7:

## How GitLens simplifies working with VS Code editor

Up until now, the tips were centered around the [GitLab Workflow extension](https://marketplace.visualstudio.com/items?itemName=GitLab.gitlab-workflow), but there is a fantastic extension that's improving VS Code git integration regardless of where you host your repository: [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens).

### 7. Walking file history

GitLens makes it easy to browse the history of changes to the current file. Each versioned file will have three new editor icons, which provides quick access to all previous revisions of the file. The middle button seen in the image below provides series of actions on the current version (e.g., opening the commit in GitLab web).

![GitLens history browsing buttons](/images/blogimages/vscode-workflows-for-working-with-gitlab/gitlens-history.png){: .shadow.medium.center}
GitLens history browsing buttons
{: .note .text-center}

### 8. Compare current HEAD against branch or tag

One of my habits was inspecting `git diff` between my feature branch and the main branch before creating an MR. More often than not, I forgot to write a test or remove some pesky `console.log()`.

GitLens adds multiple sections to your ["Source Control" tab](https://code.visualstudio.com/docs/editor/versioncontrol#_scm-providers). For each branch, tag, and commit, click a "Compare" icon which will show you changes between your current HEAD and the reference. Seeing the local diff is great for previewing changes before pushing the new branch to the remote.

![GitLens - compare with branch](/images/blogimages/vscode-workflows-for-working-with-gitlab/gitlens-compare.png){: .shadow.medium.center}
How to compare with a branch using GitLens.
{: .note .text-center}

## Everyone can contribute

We are adding new features and fixes to the GitLab Visual Studio Code editor extension every month. If you find any issues or have a feature request, please go to our [GitLab VSCode issues tracker](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues) and if your request isn't already present in the tracker, create one. Everyone can contribute to GitLab, and we welcome your ideas on how to improve our Visual Studio Code editor.
